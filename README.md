# Guia de Kong como gateway

# Versão: 1.0.0

## 1. Setup
Serão descritos os passos para iniciar uma implantação do `KONG` para uma máquina local usando [Docker-Compose](https://docs.docker.com/compose/).

#### 1.1 Instalação do Docker / Docker Compose / Kong Deck
1. Pré-Requisitos para implantação
    * Hardware Mínimos
        * 8Gb RAM
        * 20Gb HD lives
        * Processador de 2 núcleos com 2GHz
    * Software
        * Linux Ubuntu Server ou equivalente. 
    * Permissão de `root` para comandos de instalação.

1. Referências:
    * [Instalação Docker](https://docs.docker.com/engine/install/ubuntu/)
    * [Instalação Docker Compose](https://docs.docker.com/compose/install/) 
    * [Referência Dockerfile](https://docs.docker.com/engine/reference/builder/)
    * [Referência docker-compose.yml](https://docs.docker.com/compose/compose-file/)
    * [Instalação Kong Deck](https://docs.konghq.com/deck/installation/)

##### 1.1.1 Instalação

Neste ponto a versão atual do docker é a: `19.03.8`

1. Instalação do Docker em Linux baseado no `Ubuntu Server`.
    * Executar o comando abaixo visando remover versões anteriores:
        ```bash
        $ sudo apt-get remove docker docker-engine docker.io containerd runc
        ```
    
    * Executar os comando abaixo para adicionar o repositório PPA do docker:
        ```bash
        $ sudo apt-get update
        
        $ sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
        
        $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        
        $ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        ```
    
    * Executar os comandos abaixo para instalar o docker.
        ```bash
        $ sudo apt-get update
        
        $ sudo apt-get install docker-ce docker-ce-cli containerd.io
        ```

1. No servidor, caso o usuário que será usado para a aplicação não seja o `root`, executar os comandos
abaixo para conceder permissão a este usuário.

    * Criar o grupo docker.
        ```bash
        $ sudo groupadd docker
        ```
    * Adicionar o usuário desejado a este grupo.
        ```bash
        $ sudo usermod -aG docker usuário
        ``` 
        * Alterar o valor de `usuário` para o nome do usuário em questão, exemplo: `mock`.
            ```bash
            $ sudo usermod -aG docker mock
            ```
1. Após o processo de instalação, reiniciar a máquina:
    ```bash
    $ shutdown -r now
    ```
1. Testar se a instalação está OK, verificando a saída do comando abaixo.
    ```bash
    $ docker run hello-world
    ```

##### 1.1.2 Instalação do Docker Compose

Instalação do docker compose está sendo feita neste momento na versão `1.25.5`

1. Executar o comando abaixo para fazer download do docker-compose.
    ```bash
    $ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    ```

1. Executar o comando abaixo para configurar permissão de execução
    ```bash
    $ sudo chmod +x /usr/local/bin/docker-compose
    ```

1. Verificar se a instalação está OK verificando a versão na saida do comando abaixo.
    ```bash
    $ docker-compose --version
    ```

##### 1.1.3 Configuração de Proxy (Caso necessário)

Em algumas situações, o ambiente a onde esse guide line será executado pode demandar uma configuração de proxy para ter acesso a Internet.
Deve-se então seguir os passos abaixo para configurar um proxy para a instalação do docker, feita anteriormente.

Para realizar a configuração `default` de um [proxy no docker](https://docs.docker.com/network/proxy/#configure-the-docker-client) é necessário criar ou alterar o arquivo `.docker/config.json`.

1. Localizar o arquivo `config.json` em um dos locais abaixo:
    * No caso do usuário `root` o arquivo será encontrado na pasta: `/root/.docker/config.json`
    
    * No caso do usuário `não root` o arquivo será encontrado na pasta: `~/.docker/config.json`, ou seja, no `home` definido
    para o usário em questão.
    
    
1. Adicionar a configuração de proxy necessário conforme exemplo abaixo:

    ```json
    {
        "proxies": {
            "default": {
                "httpProxy":  "http://example.com:3128",
                "httpsProxy": "http://example.com:3128",
                "noProxy":    "*.host.sem.proxy,*.outro.host.sem.proxy", /* lista separada por vírgula */
                "ftpProxy":   "ftp://example.com:3128"
            }
        }
    }
    ```

#### 1.2 Implantação

1. Execução da aplicação base.                   
    * Executar os comandos abaixo para executar a instalação.
        * Realizar o `pull` das imagens Docker.
            ```bash
            $ docker-compose -f docker-kong/docker-compose.yml pull 
            ```
        * Ao final do comando a saída deve ser semelhante a informada abaixo:
            ```bash
            Pulling kong-database   ... done
            Pulling kong-migration  ... done
            Pulling kong            ... done
            Pulling konga-prepare   ... done
            Pulling konga           ... done
            Pulling mock-direto   ... done
            Pulling mock-web-bank ... done
            WARNING: Some service image(s) must be built from source by running:
                docker-compose build mock-direto mock-web-bank
            ```

        * Realizar o `build` das imagens Docker para cada container
            * mock-web-bank
                ```bash
                $ docker-compose -f docker-kong/docker-compose.yml  build --no-cache mock-web-bank 
                ```
            * mock-direto
                ```bash
                $ docker-compose -f docker-kong/docker-compose.yml  build --no-cache mock-direto 
                ```
            * beta
                ```bash
                $ docker-compose -f docker-kong/docker-compose.yml  build --no-cache beta 
                ```
            * omega
                ```bash
                $ docker-compose -f docker-kong/docker-compose.yml  build --no-cache omega 
                ```
            * No final de cada comando deve ter uma saída de sucesso semelhante a informada abaixo:
                ```bash
                ...
 
                 ---> e57702fcc3c0

                Successfully built e57702fcc3c0
                Successfully tagged mock-direto:latest
                ```
        
        * Iniciar o projeto em background.
            ```bash
            $ docker-compose -f docker-kong/docker-compose.yml  up -d 
            ```
            * A saída de sucesso deste comando será semelhante abaixo:
                ```bash

                Creating network "kong-net" with driver "bridge"
                Creating omega           ... done
                Creating mock-web-bank ... done
                Creating kong-database   ... done
                Creating beta            ... done
                Creating mock-direto   ... done
                Creating konga-prepare   ... done
                Creating kong-migration  ... done
                Creating konga           ... done
                Creating kong            ... done

                ```

## 2. Validar a implantação

1. Após a execução do processo de instalação, é necessário um tempo para as aplicações iniciarem.

1. Para verificar, executar o comando abaixo na pasta da instalação, no caso deste guide line, a pasta é `docker-kong`:
    ```bash
    $ watch docker-compose -f docker-kong/docker-compose.yml  ps 
    ```

1. Geralmente quando as aplicações estão rodando, o resultado será semelhante o informando abaixo:
    ```bash
    A cada 2,0s: docker-compose -f docker-kong/docker-compose.yml ps                                                                                                localhost.localdomain: Tue Nov 24 18:13:49 2020
    
                        Name                                  Command                  State                                   Ports
    ------------------------------------------------------------------------------------------------------------------------------------------------------------                                         
    beta              java -Djava.securi   Up             0.0.0.0:9002->9002
                    ty.egd=f ...                        /tcp              
    kong              /docker-             Up             0.0.0.0:8000->8000
                    entrypoint.sh kong                  /tcp, 0.0.0.0:8001
                    ...                                 ->8001/tcp, 0.0.0.
                                                        0:8443->8443/tcp, 
                                                        0.0.0.0:8444->8444
                                                        /tcp              
    kong-database     docker-              Up (healthy)   0.0.0.0:5432->5432
                    entrypoint.sh                       /tcp              
                    postgres                                              
    kong-migration    /docker-             Exit 0                           
                    entrypoint.sh kong                                    
                    ...                                                   
    konga             /app/start.sh        Up             0.0.0.0:1337->1337
                                                        /tcp              
    konga-prepare     /app/start.sh -c     Exit 0                           
                    prepare - ...                                         
    omega             java -Djava.securi   Up             9001/tcp, 0.0.0.0:
                    ty.egd=f ...                        9004->9004/tcp    
    mock-direto     java -Djava.securi   Up             0.0.0.0:9001->9001
                    ty.egd=f ...                        /tcp              
    mock-web-bank   java -Djava.securi   Up             0.0.0.0:9003->9003
                    ty.egd=f ...                        /tcp  
    ```

1. Na primeira implantação, esse processo pode demorar um pouco. Aguardar em média 2 minutos.
Após esse tempo caso não tenha o resultado apresentado acima, usar o comando abaixo para reiniciar um componente:
    ```bash
    $ docker-compose -f docker-kong/docker-compose.yml  restart NOME_SERVICO
    ```
    * Para o nome o valor `NOME_SERVICO` podem ser usados os valores.
        * Serviços a seguir
            * **mock-web-bank**
            * **mock-direto**
            * **beta**
            * **omega**
            * **konga**
            * **kong-database**
            * **kong**

1. Após todos os `States` serem alterados para `Up (healthy)` ou 'Up', podendo desconsiderar o `konga-prepare` e `kong-migration` basta acessar o `konga` aplicação pela URL/IP e Porta configurados.
No guide line os serviços estarão em:
    * [Mock Web Bank](http://localhost:9003/)
    * [Mock Direto](http://localhost:9001/)
    * [Beta](http://localhost:9002/)
    * [Omega](http://localhost:9004/)
    * [KONGA](http://localhost:1337/register)

1. Configurar o [KONGA](http://localhost:1337/register).
    *  Crie um usuário. Sugestão de usuário `admin` com senha `mock12345` e e-mail "admin@admin.com"
    * Na tela inicial informar algum valor no campo `name` e o endereço http://kong:8001 no campo `Kong Admin URL`.
    * Clicar em `create connection`



## 2. Criação de rotas e serviços
Os serviços e rotas no kong gateway tem como objetivo a exposição de suas API a seus clientes. No Kong Gateway, um serviço é uma entidade que representa uma API, micro-serviço

### 2.1 Exercitando
Iremos exercitar a prática de cadastrar, editar e remover serviços e rotas. Utilizaremos para a nossa prática o `cURL` e a porta administrativa do `KONG`, por padrão é a porta `8001`. Não se preocupe com esta porta agora, no item `segurança` abordaremos alternativas protegê-la. 


1. Criando o nosso primeiro serviço:

    ```bash
    $ curl -i -X POST http://localhost:8001/services \
    --data name=example_service \
    --data url='http://mockbin.org'
    ```

    * Verificando a criação do serviço:

    ```bash
    $ curl -i http://localhost:8001/services/example_service
    ```

2. Criando a nossa primeira rota:


    ```bash
    $ curl -i -X POST http://localhost:8001/services/example_service/routes \
    --data 'paths[]=/mock' \
    --data name=mocking
    ```


3. Verificando reencaminhamento do serviço:


    ```bash
    $ curl -i -X GET http://localhost:8000/mock/request
    ```

    * Ou acesse em seu navegador, clicando [aqui](http://localhost:8000/mock).

Tendo a nossa primeira experiência com o cadastro de um serviço externo, iremos agora adicionar os 4 microserviços que serão utilizados daqui para frente.

1. Mock Web Bank
   * Iremos adiconar um proxy pass para:
    https://api.qrserver.com/v1/create-qr-code/?data=OlaMundo!

    Execute 

        curl -i -X POST 'http://localhost:8001/services' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "url" : "https://api.qrserver.com",
            "name" : "qrcode",
            "retries" : 5,
            "connect_timeout" : 60000,
            "write_timeout" : 60000,
            "read_timeout" : 60000
        }'

Resultado esperado

    HTTP/1.1 201 Created
    Date: Thu, 26 Nov 2020 19:29:50 GMT
    Content-Type: application/json; charset=utf-8

Validando a criação de todos os serviços até agora:

curl --location --request GET 'http://localhost:8001/services'

Resultado esperado

    {"next":null,"data":[{"host":"mockbin.org","id":"1f76c5b7-6f59-43aa-aa16-ffff000ce118","protocol":"http","read_timeout":60000,"tls_verify_depth":null,"port":80,"updated_at":1606419202,"ca_certificates":null,"created_at":1606419202,"connect_timeout":60000,"write_timeout":60000,"name":"example_service","retries":5,"path":null,"tls_verify":null,"tags":null,"client_certificate":null},{"host":"api.qrserver.com","id":"8035c4f6-ac3c-4c29-83f5-37504f624f08","protocol":"https","read_timeout":60000,"tls_verify_depth":null,"port":443,"updated_at":1606421017,"ca_certificates":null,"created_at":1606421017,"connect_timeout":60000,"write_timeout":60000,"name":"qrcode","retries":5,"path":null,"tls_verify":null,"tags":null,"client_certificate":null}]}

* Adicionado a rota `qrcode':
  
    curl --location --request POST 'http://localhost:8001/services/qrcode/routes' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "protocols": [
            "http",
            "https"
        ],
        "paths": [
            "/qrcode"
        ],
        "methods": []
    }'

* Valide o redirecionamento em http://localhost:8000/qrcode/v1/create-qr-code/?data=OlaMundo!

* Agora vamos editar o nosso serviço e pedir para `mock web bank` tratar estas requisições, para isso execute:
    
        curl -i -X PATCH 'http://localhost:8001/services/qrcode' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "url" : "http://mock-web-bank:9003/",
            "name" : "qrcode",
            "retries" : 50,
            "connect_timeout" : 60000,
            "write_timeout" : 60000,
            "read_timeout" : 60000
        }'

Retorno esperado:

    HTTP/1.1 200 OK
    Date: Thu, 26 Nov 2020 20:12:07 GMT
    Content-Type: application/json; charset=utf-8

* Verifique o novo resultado em http://localhost:8000/qrcode/v1/create-qr-code/?data=OlaMundo! 

* Logue na interface do [KONGA](http://localhost:1337) com as credencias criadas no `Setup` e:
  * Clique em `services`
  * Clique em `ADD NEW SERVICE`
  * Preencha o formulário com:
    * No campo `name` informar o valor `mock-web-bank`
    * No campo `Url` informar o valor `http://mock-web-bank:9003`
    * Nos campos restantes deve permanecer o valor padrão.
    * Clique em `Submit Service`
  * 

Até então utilizamos o recurso da [API do KONG](https://docs.konghq.com/2.2.x/admin-api/). Daqui em diante utilizaremos o [KONGA](https://pantsel.github.io/konga/).

Primeiro vamos adicionar os serviços que serão utilizados no restante deste guia:

1. Logue na interface do [KONGA](http://localhost:1337) com as credencias criadas no `Setup` e:
2. Para adicionar novos serviços clique em `services` e:
   * Clique em `ADD NEW SERVICE` e preencha o formulário com para o `mock-direto`:
     * No campo `name` informar o valor `mock-direto`
     * No campo `Url` informar o valor `http://mock-direto:9001`
     * Nos campos restantes deve permanecer o valor padrão.
     * Clique em `Submit Service`
   * Clique em `ADD NEW SERVICE` e preencha o formulário com para o `beta`:
     * No campo `name` informar o valor `beta`
     * No campo `Url` informar o valor `http://beta:9002`
     * Nos campos restantes deve permanecer o valor padrão.
     * Clique em `Submit Service`
   * Clique em `ADD NEW SERVICE` e preencha o formulário com para o `mock-web-bank`:
     * No campo `name` informar o valor `mock-web-bank`
     * No campo `Url` informar o valor `http://mock-web-bank:9003`
     * Nos campos restantes deve permanecer o valor padrão.
     * Clique em `Submit Service`
   * Clique em `ADD NEW SERVICE` e preencha o formulário com para o `omega`:
     * No campo `name` informar o valor `omega`
     * No campo `Url` informar o valor `http://omega:9004`
     * Nos campos restantes deve permanecer o valor padrão
     * Clique em `Submit Service`
3. Agora vamos adicionar as rotas para os serviços.
   * Na tela `Services` clique no serviço `mock-direto` e:
     * Clique em `routes`
     * Clique em `ADD ROUTE` e preencha o formulário:
       * No campo `name` informar o valor `mock-direto-route`
       * No campo `Paths` informar o valor `/mock-direto` e pressione `enter`
       * Clique em `SUBMIT ROUTE`
       * Clique em `services`
       * Nos campos restantes deve permanecer o valor padrão
   * Na tela `Services` clique no serviço `beta` e:
     * Clique em `routes`
     * Clique em `ADD ROUTE` e preencha o formulário:
       * No campo `name` informar o valor `beta-route`
       * No campo `Paths` informar o valor `/beta` e pressione `enter`
       * Clique em `SUBMIT ROUTE`
       * Clique em `services`
       * Nos campos restantes deve permanecer o valor padrão
   * Na tela `Services` clique no serviço `mock-web-bank` e:
     * Clique em `routes`
     * Clique em `ADD ROUTE` e preencha o formulário:
       * No campo `name` informar o valor `mock-web-bank-route`
       * No campo `Paths` informar o valor `/mock-web-bank` e pressione `enter`
       * Clique em `SUBMIT ROUTE`
       * Clique em `services`
       * Nos campos restantes deve permanecer o valor padrão
   * Na tela `Services` clique no serviço `mock-direto` e:
     * Clique em `routes`
     * Clique em `ADD ROUTE` e preencha o formulário:
       * No campo `name` informar o valor `mock-direto-route`
       * No campo `Paths` informar o valor `/mock-direto` e pressione `enter`
       * Clique em `SUBMIT ROUTE`
       * Clique em `services`
       * Nos campos restantes deve permanecer o valor padrão
  1. Se tudo deu certo até aqui, ao acessar as `URL` a baixo, você verá o nome do sistema:
      * [Mock Direto](http://localhost:8000/mock-direto)
      * [Mock Web Bank](http://localhost:8000/mock-web-bank)
      * [Beta](http://localhost:8000/beta)
      * [Omega](http://localhost:8000/omega)
   1. Crie um Snapshot do que fizemos até agora
      * Clique em `SNAPSHOTS`
      * Clique então em `INSTANT SNAPSHOT`
      * No campo `Give the snapshot a unique name` informe o valor `guide-line`
      * No item `guide-line` clique em `DETAILS`
      * Na tela `Snapshot Details` clique em `Export`
      * Verifique em sua pasta de downloads a presença do arquivo `snapshot_1.json`
   1. (Opcional) __Apenas execute este item se você tiver feito o snapshot do passo anterior__. Vamos restaurar um Snapshot.
      * Pare os containers `kong` e `kong-database`
        ```bash
        $ docker stop kong kong-database
        ```
      * Remova os containers `kong` e `kong-database`
        ```bash
        $ docker rm kong kong-database
        ```
      * Recrie os containers `kong` e `kong-database`
        ```bash
        $ docker-compose -f docker-kong/docker-compose.yml up -d
        ```
      * Volte a tela do [KONGA](http://localhost:1337/) e o configure novamente:
        *  Crie um usuário. Sugestão de usuário `admin` com senha `mock12345` e e-mail "admin@admin.com"
        * Na tela inicial informar algum valor no campo `name` e o endereço http://kong:8001 no campo `Kong Admin URL`.
        * Clicar em `create connection`
      * Clique em `Services` para constatar que você está em uma nova instalação.
      * Clique em `Snapshot`
      * Clique em `IMPORT FROM FILE` e selecione o arquivo `snapshot_1.json`
      * No novo item que apareceu na tabela, clique em `DETAILS`
      * Na tela `Snapshot Details` clique em `RESTORE` e selecione todas as opções
      * Selecione todas as opções e clique em `IMPORT OBJECTS`
      * É possível que o passo anterior falhe e mostre algum número diferente de *0* na coluna `Failed`. Caso isso ocorra, clique em `RESTORE` selecione todas as opções e clique em `IMPORT OBJECTS` novamente.
      * Clique no menu `Services` e verá que o `Snapshot` foi restaurado.

Existem ainda os conceitos de `UPSTREAM` uteis quando existe a necessidade de criar balanceamento de carga entre instâncias da mesma `API`. Como o `Kong` não será responsável por balancear `API`, este conceito não será abordado neste guia.




### 2.2 Recomendações

É uma boa prática para gerenciamento de configuração garantir replicabilidade, este e outros temas serão abordados com maior profundidade em `Boas Práticas em Implantação`.

Seria possível garantir a replicabilidade até com um arquivo em `shell script` orquestrando comandos `cURL`, por exemplo. Porém, soluções do tipo "faça você mesmo" trazem com elas diversas complexidades, como o gerenciamento de estado do seu `Kong Gateway`.

Existe também a possibilidade de criar rotinas agendáveis no `KONGA` que criará snapshots conforme programado. É uma opção interessante caso se exista a cultura de gerenciar as configurações via interface do `KONGA`.

Para gerenciamento do seu `Kong Gateway`, recomendamos a utilização do `deck`. O deck é uma ferramenta mantida pela `KONG` que, entre outras coisas, tem a habilidade de tirar uma foto (ou snapshot) do estado atual do seu cluster salvando estas informações em um arquivo de texto aberto. Posterior a criação do arquivo, você pode pedir para o `deck` sincronizar o seu cluster. O `deck` também está disponível através da imagem `kong/deck`. Com essa imagem uma integração no pipeline é facilitada, viabilizando assim um cenário a onde alterações seriam primeiro validadas em um ambiente controlado, transferida para um arquivo `yml` e então encorporadas em um pipeline e aplicada no(s) ambiente(s). 

Com o `deck` instalado, vamos exercitar.

1. Verifique a conexão com o seu servidor:

    ```bash
    $ deck ping
    ```

   * A saída de sucesso deste comando será semelhante a baixo:
  
    ```bash
    Successfully connected to Kong!
    Kong version:  2.1.0
    ```
2. Salvando o estado do kong:

    ```bash
    $ deck dump
    ```
   * Este comando não tem uma saída de sucesso, e sim a criação do arquivo `kong.yaml` com o conteúdo semelhante a baixo:

        ```yaml
        _format_version: "1.1"
        services:
        - connect_timeout: 60000
        host: mockbin.org
        name: example_service
        port: 80
        protocol: http
        read_timeout: 60000
        retries: 5
        write_timeout: 60000
        routes:
        - name: mocking
            paths:
            - /mock
            path_handling: v0
            preserve_host: false
            protocols:
            - http
            - https
            regex_priority: 0
            strip_path: true
            https_redirect_status_code: 426
        ```
3. Verificar a diferença entre o arquivo `kong.yaml` e o servidor `kong`:
   ```bash
    $ deck diff
    ```

   * A saída de sucesso deste comando será semelhante a baixo:
  
    ```bash
    Summary:
        Created: 0
        Updated: 0
        Deleted: 0
    ```

4. Sincronizar:
   ```bash
    $ deck sync
    ```

   * A saída de sucesso deste comando será semelhante a baixo:
  
    ```bash
    Summary:
        Created: 0
        Updated: 0
        Deleted: 0
    ```
5. Vamos então remover a rota `mocking` criado no item 2.1:
   ```bash
    $ curl -i -X  DELETE 'http://localhost:8001/routes/mocking'
    ```
6. Verificar a diferença entre o arquivo `kong.yaml` e o servidor `KONG`:
   ```bash
    $ deck diff
    ```

   * A saída de sucesso deste comando será semelhante a baixo:
  
    ```bash
    creating route mocking
    Summary:
        Created: 1
        Updated: 0
        Deleted: 0
    ```
7. Sincronizar após a deleção da rota `mocking`:
   ```bash
    $ deck sync
    ```

   * A saída de sucesso deste comando será semelhante a baixo:
  
    ```bash
    creating route mocking
    Summary:
        Created: 1
        Updated: 0
        Deleted: 0
    ```

Para manutenção do `Kong Gateway`, recomendamos a utilização da ferramenta [postman](https://www.postman.com/downloads/). O `postman` tem a funcionalidade na versão APP de `coleções`, com ela você pode centralizar a criação de suas requisições, reaproveitando-as, categorizando-as, utilizando recursos como variáveis para o id, ip do servidor, teste de carga, entre outros.

## 3. Transformação de dados
No `KONG` temos uma boa gama de plugins. Um em especial lhe proporciona a opção de fazer a transformação de dados, através da transformação de request. Com esta opção, você poderia por exemplo excluir itens desnecessário, adicionar, renomear, tanto na requisição quanto na resposta. O `Kong` ainda tem uma opção de correlacionar com o mesmo identificador a requisição e a resposta, util por exemplo para melhorar o nível de rastreio. Vamos colocar em prática?

1. Transformando uma requisição
    * Execute a requisição a baixo:
   ```bash
    curl --location --request POST 'http://localhost:8000/mock-direto/api/v1/echo?param-1=val-1&param-2=val-2' \
    --header 'Content-Type: application/json' \
    --header 'header-1: header-val-1' \
    --data-raw '{
        "test" : "Pix, por favor"
    }'
    ```

    * O resultado esperado é semelhante:
   ```bash
    Request body is:
    {
        "test" : "Pix, por favor"
    }

    -----------------------------------------------------------------------
    HTTP request headers are:
    host : org.apache.tomcat.util.http.ValuesEnumerator@7cbaa6c2
    connection : org.apache.tomcat.util.http.ValuesEnumerator@727d4725
    x-forwarded-for : org.apache.tomcat.util.http.ValuesEnumerator@7642cdb2
    x-forwarded-proto : org.apache.tomcat.util.http.ValuesEnumerator@33404d1e
    x-forwarded-host : org.apache.tomcat.util.http.ValuesEnumerator@1ae57102
    x-forwarded-port : org.apache.tomcat.util.http.ValuesEnumerator@2395914
    x-forwarded-prefix : org.apache.tomcat.util.http.ValuesEnumerator@62763bbc
    x-real-ip : org.apache.tomcat.util.http.ValuesEnumerator@536fb251
    content-length : org.apache.tomcat.util.http.ValuesEnumerator@6233a45f
    user-agent : org.apache.tomcat.util.http.ValuesEnumerator@2a8ab0f4
    accept : org.apache.tomcat.util.http.ValuesEnumerator@7972d5c2
    content-type : org.apache.tomcat.util.http.ValuesEnumerator@57210d87
    header-1 : org.apache.tomcat.util.http.ValuesEnumerator@284f4a60


    -----------------------------------------------------------------------
    HTTP query params are:
    param-1=val-1&param-2=val-2
    ```

    * Agora entre no `KONGA` e clique em `Services`
    * Selecione `mock-direto`
    * Clique em `Plugins` e `ADD PLUGIN`
    * Clique na aba `Transformations`
    * Em `Request Transformer` clique em `ADD PLUGIN` e:
        * Em `remove` para o item `querystring` entre com o valor `param-1` e pressione `enter`
        * Em `rename` para o item `querystring` entre com o valor `param-2:pix-hoje` e pressione `enter`
        * Em `replace` para o item `headers` entre com o valor `header-1:bem-vindo` e pressione `enter`
        * Em `add` para o item `body` entre com o valor `fonte:kong` e pressione `enter`
        * Em `add` para o item `headers` entre com o valor `usuário:infinite` e pressione `enter`
        * Em `add` para o item `headers` entre com o valor `usuário:infinite` e pressione `enter`
        * Em `append` para o item `body` entre com o valor `pix:sim` e pressione `enter`
        * Cliquem em `ADD PLUGIN`
    * Execute novamente:
   ```bash
    curl --location --request POST 'http://localhost:8000/mock-direto/api/v1/echo?param-1=val-1&param-2=val-2' \
    --header 'Content-Type: application/json' \
    --header 'header-1: header-val-1' \
    --data-raw '{
        "test" : "Pix, por favor"
    }'
    ```
    * O resultado esperado é semelhante:
   ```bash
   Request body is:
    {"fonte":"kong","test":"Pix, por favor","pix":["sim"]}

    -----------------------------------------------------------------------
    HTTP request headers are:
    host : org.apache.tomcat.util.http.ValuesEnumerator@308be9f3
    connection : org.apache.tomcat.util.http.ValuesEnumerator@7ee5f21b
    x-forwarded-for : org.apache.tomcat.util.http.ValuesEnumerator@1ad0d8bc
    x-forwarded-proto : org.apache.tomcat.util.http.ValuesEnumerator@44a41f11
    x-forwarded-host : org.apache.tomcat.util.http.ValuesEnumerator@2b8fa207
    x-forwarded-port : org.apache.tomcat.util.http.ValuesEnumerator@7aabe847
    x-forwarded-prefix : org.apache.tomcat.util.http.ValuesEnumerator@66a2da96
    x-real-ip : org.apache.tomcat.util.http.ValuesEnumerator@29a0dc4
    content-length : org.apache.tomcat.util.http.ValuesEnumerator@54d3180a
    user-agent : org.apache.tomcat.util.http.ValuesEnumerator@719f2b07
    accept : org.apache.tomcat.util.http.ValuesEnumerator@11929c8
    content-type : org.apache.tomcat.util.http.ValuesEnumerator@53bff002
    header-1 : org.apache.tomcat.util.http.ValuesEnumerator@1f60a137
    usuário : org.apache.tomcat.util.http.ValuesEnumerator@17af66cf


    -----------------------------------------------------------------------
    HTTP query params are:
    pix-hoje=val-2
    ```
1. Transformando um reponse
    * Apague o plugin anterior
    * Execute
    ```bash
    curl --location --request GET 'http://localhost:8000/mock-direto/api/v1/employee' --header 'Content-Type: application/json'
    ```
    * O resultado esperado é semelhante a:
    ```bash
    {"email":"artemis@athena.com","name":"Artemis","salary":5930}
    ```
    
    * Agora entre no `KONGA` e clique em `Services`
    * Selecione `mock-direto`
    * Clique em `Plugins` e `ADD PLUGIN`
    * Clique na aba `Transformations`
    * Em `Response Transformer` clique em `ADD PLUGIN` e:
        * Em `remove` para o item `json` entre com o valor `test` e pressione `enter`
        * Em `rename` para o item `querystring` entre com o valor `param-2:pix-hoje` e pressione `enter`
        * Em `add` para o item `json` entre com o valor `nome:Maria` e pressione `enter`
        * Em `add` para o item `headers` entre com o valor `pix-situacao:Cadastrado` e pressione `enter`
        * Em `append` para o item `body` entre com o valor `nome: Madalena` e pressione `enter`
        * Cliquem em `ADD PLUGIN`
    * Execute
    ```bash
    curl --location --request GET 'http://localhost:8000/mock-direto/api/v1/employee' --header 'Content-Type: application/json'
    ```
    * O resultado esperado é semelhante a:
    ```bash
    {"salary":5930,"name":["Artemis"," Madalena"],"pix-situacao":"Cadastrado","email":"artemis@athena.com"}
    ```
1. Agora veremos o `Correlation Id`
    * Remova o plugin do passo anterior
    * Execute a requisição a baixo:
   ```bash
    curl --location --request POST 'http://localhost:8000/mock-direto/api/v1/echo?param-1=val-1&param-2=val-2' \
    --header 'Content-Type: application/json' \
    --header 'header-1: header-val-1' \
    --data-raw '{
        "test" : "Pix, por favor"
    }'
    ```

    * O resultado esperado é semelhante:
   ```bash
    Request body is:
    {
        "test" : "Pix, por favor"
    }

    -----------------------------------------------------------------------
    HTTP request headers are:
    host : org.apache.tomcat.util.http.ValuesEnumerator@7cbaa6c2
    connection : org.apache.tomcat.util.http.ValuesEnumerator@727d4725
    x-forwarded-for : org.apache.tomcat.util.http.ValuesEnumerator@7642cdb2
    x-forwarded-proto : org.apache.tomcat.util.http.ValuesEnumerator@33404d1e
    x-forwarded-host : org.apache.tomcat.util.http.ValuesEnumerator@1ae57102
    x-forwarded-port : org.apache.tomcat.util.http.ValuesEnumerator@2395914
    x-forwarded-prefix : org.apache.tomcat.util.http.ValuesEnumerator@62763bbc
    x-real-ip : org.apache.tomcat.util.http.ValuesEnumerator@536fb251
    content-length : org.apache.tomcat.util.http.ValuesEnumerator@6233a45f
    user-agent : org.apache.tomcat.util.http.ValuesEnumerator@2a8ab0f4
    accept : org.apache.tomcat.util.http.ValuesEnumerator@7972d5c2
    content-type : org.apache.tomcat.util.http.ValuesEnumerator@57210d87
    header-1 : org.apache.tomcat.util.http.ValuesEnumerator@284f4a60


    -----------------------------------------------------------------------
    HTTP query params are:
    param-1=val-1&param-2=val-2
    ```

    * Agora entre no `KONGA` e clique em `Services`
    * Selecione `mock-direto`
    * Clique em `Plugins` e `ADD PLUGIN`
    * Clique na aba `Transformations`
    * Em `Correlation Id` clique em `ADD PLUGIN` e:
        * Em `header name` entre com o valor `X-ID-CORRELACAO`
        * Em `generator`  selecione valor `uuid`
        * Em `echo downstream` marque `ON`
        * Clique em `ADD PLUGIN`
    * Execute a requisição a baixo:
   ```bash
    curl --location --request POST 'http://localhost:8000/mock-direto/api/v1/echo?param-1=val-1&param-2=val-2' \
    --header 'Content-Type: application/json' \
    --header 'header-1: header-val-1' \
    --data-raw '{
        "test" : "Pix, por favor"
    }'
    ```
    * O resultado esperado é semelhante a:
   ```bash
    Request body is:
    {
        "test" : "Pix, por favor"
    }

    -----------------------------------------------------------------------
    HTTP request headers are:
    host : org.apache.tomcat.util.http.ValuesEnumerator@5f304a9b
    connection : org.apache.tomcat.util.http.ValuesEnumerator@73a39e9f
    x-forwarded-for : org.apache.tomcat.util.http.ValuesEnumerator@6950f0c5
    x-forwarded-proto : org.apache.tomcat.util.http.ValuesEnumerator@5257a3e
    x-forwarded-host : org.apache.tomcat.util.http.ValuesEnumerator@589c9c93
    x-forwarded-port : org.apache.tomcat.util.http.ValuesEnumerator@3724a679
    x-forwarded-prefix : org.apache.tomcat.util.http.ValuesEnumerator@591cbe80
    x-real-ip : org.apache.tomcat.util.http.ValuesEnumerator@7746cb17
    content-length : org.apache.tomcat.util.http.ValuesEnumerator@4b82a968
    user-agent : org.apache.tomcat.util.http.ValuesEnumerator@5907d90f
    accept : org.apache.tomcat.util.http.ValuesEnumerator@65539411
    content-type : org.apache.tomcat.util.http.ValuesEnumerator@248cf5df
    header-1 : org.apache.tomcat.util.http.ValuesEnumerator@7a1bc1cf
    x-id-correlacao : org.apache.tomcat.util.http.ValuesEnumerator@1d33b022


    -----------------------------------------------------------------------
    HTTP query params are:
    param-1=val-1&param-2=val-2
    ```

## 4. Autenticação
O [Kong] open source nos dá várias soluções para trabalhar com autenticação. Dentre elas iremos abordar neste guia
`Basic Authentication`, `Api Key`, `JWT` e `OAuth 2`.
- Basic Authentication: Será necessário um `usuário`e `senha` para acessar a API.
- Api Key: Será utilizado uma palavra(string), geralmente no cabeçalho da requisição para acessar a API.
- JWT: Será utilizado uma token JWT(JSON Web Token). Um token JWT é composto de cabeçalho, corpo e assinatura. Estes campos são separados por "`.`"(ponto). 
- OAuth 2: É uma especificação com diversos fluxos indicados para diferentes cenários. Basicamente essa especificação irá delegar a componentes externos parte da complexidade envolvida em autenticação.




Não abordaremos aqui o [Kong] como solução para remover a totalmente a autenticação das aplicações. Seguem alguns plugins que se propõem a isso.

- [Kong enterprise plugin](https://docs.konghq.com/hub/kong-inc/openid-connect/)
- https://github.com/gbbirkisson/kong-plugin-jwt-keycloak
- https://github.com/nokia/kong-oidc

### 4.1. Basic Authentication
1. No `KONGA` clique em `PLUGINS`
1. Clique em `ADD GLOBAL PLUGIN`
1. Encontre o item `Basic Auth`, clique no botão `ADD PLUGIN` e preencha o formulário: 
    * Deixe os valores padrões e clique em `ADD PLUGIN`
1. Tente chamar a api `mock-direto` que configuramos, executando: 

   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-direto'
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    {
    "message":"Unauthorized"
    }
    ```
1. De volta ao `KONGA`, clique em `CONSUMERS`
1. Clique em `CREATE CONSUMER` e preencha o formulário:
    * Em `username` entre com o valor `marcelo`
    * Clique em `SUBMIT CONSUMER`
1. Clique em `Credentians`
1. Em `Basic Auth` clique em `CREATE CREDENTIALS` e preencha o formulário:
    * No campo `username` informe o valor `user-marcos`
    * No campo `password` informe o valor `pass-marcos`
    * Clique no botão `SUBMIT`
1. Tente chamar a api `mock-direto` que configuramos, executando: 

   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-direto' \
    --header 'Authorization: Basic dXNlci1tYXJjb3M6cGFzcy1tYXJjb3M='
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    Mock Direto
    ```
1. Apague o plugin global criado

### 4.2. Api Key
1. No `KONGA` selecione `SERVICOS`
1. Clique o serviço `mock-direto`
1. Clique em `Plugins` e em `ADD PLUGIN`
1. Encontre o plugin `Key Auth` e clique em `ADD PLUGIN` e preencha o formulário:
    * No campo `key names` informe o valor `chave-api` e pressione `enter`
    * Clique em `ADD PLUGIN`
1. Tente chamar a api `mock-direto` que configuramos, executando: 

   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-direto'
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    {
    "message":"No API key found in request"
    }
    ```
1. Em `CONSUMER` selecione o consumidor `marcos`
1. Clique em `Credentials` e clique em `API KEYS`
1. Clique em `CREATE API KEY` e preencha o formulário:
    * No campo `key` informe o valor `chave-marcos`
    * Clique em `SUBMIT
1. Tente chamar a api `mock-direto` passando a chave de api e os valores que criamos, executando: 

   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-direto' \
    --header 'chave-api: chave-marcos'
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    Mock Direto
    ```
1. Apague o plugin criado no serviço `mock-direto`

### 4.3 JWT
1. No `KONGA` selecione `SERVICOS`
1. Clique o serviço `mock-direto`
1. Clique em `Plugins` e em `ADD PLUGIN`
1. Encontre o plugin `Jwt` e clique em `ADD PLUGIN` e preencha o formulário:
    * No campo `claims to verify` informe o valor `exp` e pressione `enter`
    * No campo `maximum expiration` informe o valor `31536000`(1 ano)
    * Clique em `ADD PLUGIN`
1. Tente fazer uma requisição para `mock-direto`
   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-direto'
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    {
        "message": "Unauthorized"
    }
   ```
1. No `KONGA` selecione `CONSUMERS`
1. Clique no consumidor `marcos`
1. Clique em `Credentials` e em `JTW`
1. Clique em `CREATE JWT` e preencha o formulário:
    * No campo `key` informe o valor `marcos-key`
    * No campo `secret` informe o valor `marcos-secret`
    * Clique no botão `SUBMIT`
1. Para calcular o valor do JWT convertido, você pode ir até o [jwt.io](): 
   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-direto' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaXNzIjoibWFyY29zLWtleSIsImV4cCI6MTYzODA2OTQ5Nn0.uCUt5rqlw_2goFUHUJ_W-H3WkGuV0F6igNv76TqpDKE'
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    Mock Direto
   ```
1. Apague o plugin criado neste item.
### 4.4. OAuth 2
1. No `KONGA` selecione `SERVICOS`
1. Clique o serviço `mock-web-bank`
1. Clique em `Plugins` e em `ADD PLUGIN`
1. Encontre o plugin `Oauth2` e clique em `ADD PLUGIN` e preencha o formulário:
    * No campo `provision key` informe o valor `provisionkey`
    * No campo `enable client credentials` marque `YES`
    * Clique em `ADD PLUGIN`
1. No `KONGA` selecione `CONSUMERS`
1. Clique no consumidor `marcos`
1. Clique em `Credentials` e em `OAUTH2`
1. Clique em `CREATE CREDENTIALS` e preencha o formulário:
    * No campo `name` informe o valor `marcos-app`
    * No campo `client_id` informe o valor `marcos-clientid`
    * No campo `client_secret` informe o valor `marcos-clientsecret`
    * No campo `redirect_uris` informe o valor `http://localhost:8000/mock-direto` e pressione `enter`
    * Clique no botão `SUBMIT
2. Tente fazer uma requisição para `mock-web-bank`
   ```bash    
    curl --location --request GET 'http://localhost:8000/mock-web-bank'
    ```
    * Retorno esperado é semelhante a: 
   ```bash    
    {"error_description":"The access token is missing","error":"invalid_request"}
   ```   
3. Obtenha um `acess token`, execute:
   ```bash
   curl    -k  -X POST 'https://localhost:8443/mock-web-bank/oauth2/token' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "client_id" : "marcos-clientid",
        "client_secret" : "marcos-clientsecret",
        "grant_type" : "client_credentials"
    }'
   ```   
    * O retorno esperado é semelhante a: 
   ```bash
    {"token_type":"bearer","access_token":"5FTjLHYJ2XRQM274ZDtgu2wgu4ztE0sV","expires_in":7200}
   ```   
   * Utilizando o acess_token, execute:

   ```bash
    curl --location --request GET 'http://localhost:8000/mock-web-bank' \
    --header 'Authorization: Bearer 5FTjLHYJ2XRQM274ZDtgu2wgu4ztE0sV' \
    --data-raw ''
   ```
   * O retorno esperado é semelhante a:
   ```bash
   Mock Web Bank
   ```  
4. Agora vamos utilizar o fluxo `Password`
5. No `KONGA`vá até `PLUGINS` e o edite
   * No campo `enable client credentials` marque `NO`
   * No campo `enable password grant` marque `YES`
1. Execute a requisição agora com `grant_type` com valor `password`

   ```bash
    curl -k -X POST 'https://localhost:8443/mock-web-bank/oauth2/token' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "client_id": "marcos-clientid",
        "client_secret": "marcos-clientsecret",
        "authenticated_userid": "marcos",
        "grant_type": "password",
        "provision_key": "provisionkey"
    }'
   ``` 
    * O retorno esperado é semelhante a:
   ```bash
   {"refresh_token":"KK5hk4FENVmjjbKMaxnkFJM2iOorxHaw","token_type":"bearer","access_token":"PEbDQc57HVRtQDPROLtezAsCywZAHP2q","expires_in":7200}
   ``` 
   * Utilizando o acess_token, execute:

   ```bash
    curl --location --request GET 'http://localhost:8000/mock-web-bank' \
    --header 'Authorization: Bearer PEbDQc57HVRtQDPROLtezAsCywZAHP2q' \
    --data-raw ''
   ```
   * O retorno esperado é semelhante a:
   ```bash
   Mock Web Bank
   ```     
   * Utilizando o `refresh_token` para obter um novo token:

   ```bash
    curl -k -X POST 'https://localhost:8443/mock-web-bank/oauth2/token' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "grant_type": "refresh_token",
        "refresh_token" : "KK5hk4FENVmjjbKMaxnkFJM2iOorxHaw",
        "client_id" : "marcos-clientid",
        "client_secret" : "marcos-clientsecret"
    }'
   ```
   * O retorno esperado é semelhante a:
   ```bash
   {"refresh_token":"PGRO6zdGnQU9uBGaSvL2HKFeXW565Mip","token_type":"bearer","access_token":"SDKisfvdJYL7kPXWwOmvehfV1qQu1znr","expires_in":7200}
   ```     
1. Apague o plugin `oauth 2`

## 5. Segurança
Ao falar em segurança para o `KONG`, a primeira preocupação é a sua interface administrativa, por padrão sobe na porta `8001`. Existem vários meios de protegêla, vamos no guia mostrar a proteção via chave de API com privilégios de grupo.

Ainda no tema segurança, o `KONG` tem suporte a restrição por `IP` e `user-agent`, que também será explicitado a seguir.

### 5.1. Proteção da interface administrativa
1. Vamos criar um serviço para expor o `KONG-ADMIN` disponível no endereço http://localhost:8001
    * Para adicionar novos serviços clique em `services` e:
        * Clique em `ADD NEW SERVICE` e preencha o formulário com para o `kong-admin`:
        * No campo `name` informar o valor `kong-admin`
        * No campo `Url` informar o valor `http://localhost:8001`
        * Nos campos restantes deve permanecer o valor padrão.
        * Clique em `Submit Service`
    * Na tela de `services`, clique no serviço `kong-admin`
    * Clique em `ADD ROUTE` e preencha o formulário:
       * No campo `name` informar o valor `kong-admin-route`
       * No campo `Paths` informar o valor `/admin` e pressione `enter`
       * Nos campos restantes deve permanecer o valor padrão
       * Clique em `SUBMIT ROUTE`
2. Agora o endereço http://localhost:8000/admin irá retornar a api administrativa do `KONG`
3. Vamos adicionar uma autenticação de `API` para a api administrativa:
    * Clique no menu `CONSUMERS`
    * Cliquem em `CREATE CONSUMER` e preencha o formulário:
        * No campo `username` informe o valor `kong-admin` e clique em `SUBMIT CONSUMER`
        * Clique na aba `Credencials`
        * Clique em `API KEYS`
        * Clique em `CREATE API KEY` e:
            * No campo `key` informe o valor `kong-admin-key`
            * Clique em `SUBMIT`
4. Temos também que atualizar a connections do `Konga` para utilizar a nova `key`
    * Clique em `CONNECTIONS`
    * Clique em `docker` ou o nome que tenha dado a sua conexão e:
        * Clique na aba `KEY AUTH`
        * No campo `Kong Admin URL` informe o valor `http://kong:8000/admin`
        * No campo `API KEY` informe o valor `kong-admin-key`
        * Clique em `UPDATE CONNECTION`
5. Agora vamos adicionar a autenticação no serviço `kong-admin`
    * Clique em `SERVICES`
    * Selecione `kong-admin`
    * Clique em `Plugins` e `ADD PLUGIN`
    * Encontre o plugin `Key Auth` e clique em `ADD PLUGIN` e preencha o formulário:
        * No campo `key names` informe o valor `apikey`
        * Clique em `ADD PLUGIN`
6. O caminho http://localhost:8000/admin não está mais disponível via browser 
    * Para acessar o admin é necessário uma chave de api no cabeçalho, como mostrado a seguir:

   ```bash
    curl --location --request GET 'http://localhost:8000/admin' \
    --header 'apikey: kong-admin-key'
    ```
7. Mas e se alguém com uma outra chave de api válida tentar acessar o serviço `kong-admin`?
    * Para simular isso vamos criar um consumer, clique em `CONSUMERS` e `CREATE CONSUMER`
    * No campo nome preencha `n-admin` e clique em `SUBMIT CONSUMER`
    * Clique em `Credentials`
    * Clique em `API KEY` e `CREATE API KEY` e preencha o formulário:
        * No campo `key` informe o valor `n-admin-key`
        * Clique em `SUBMIT`
    * Execute então
   ```bash    
    curl -I -X GET 'http://localhost:8000/admin' --header 'apikey: n-admin-key'
    ```
     * O retorno será algo semelhante a:
   ```bash         
    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8
    Content-Length: 9841
    Connection: keep-alive
    Date: Fri, 27 Nov 2020 18:21:43 GMT
    Access-Control-Allow-Origin: *
    Server: kong/2.1.4
    X-Kong-Admin-Latency: 13
    X-Kong-Upstream-Latency: 14
    X-Kong-Proxy-Latency: 0
    Via: kong/2.1.4
    ```

8. Neste ponto, qualquer um com o segredo da chave de api poderia acessar, para se tornar realmente seguro, iremos combinar a chave de api com restrição de acesso
    * Na tela `consumers` selecione `kong-admin`
    * Clique em `Groups` e clique em `Add a group`
        * No campo `Group name` entre com o valor `administrador` e clique em `Submit Group`
    * Vá até a tela de `SERVICES`
    * Clique em `kong-admin`
    * Clique em `plugin` e cliquem em `ADD PLUGIN`
    * Selecione a aba `Security`
    * Encontre o plugin `ACL`e clique em `ADD PLUGIN`
        * No campo `allow` entre com o valor `administrador`
        * Clique em `ADD PLUGIN`
    * Ao executar uma requisição com uma chave que não seja do grupo `administrador`, será lançado um erro `403`
    ```bash    
    curl -I -X GET 'http://localhost:8000/admin' --header 'apikey: n-admin-key'
    ```
    * O retorno será algo semelhante a:
   ```bash         
    HTTP/1.1 403 Forbidden
    Date: Fri, 27 Nov 2020 18:25:42 GMT
    Content-Type: application/json; charset=utf-8
    Connection: keep-alive
    Content-Length: 49
    X-Kong-Response-Latency: 3
    Server: kong/2.1.4
    ```
    * E a chave `` continuará funcionando
    ```bash    
    curl -I -X GET 'http://localhost:8000/admin' --header 'apikey: kong-admin-key'
    ```
    * O retorno será algo semelhante a:
    ```bash    
    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8
    Content-Length: 9847
    Connection: keep-alive
    Date: Fri, 27 Nov 2020 18:27:47 GMT
    Access-Control-Allow-Origin: *
    Server: kong/2.1.4
    X-Kong-Admin-Latency: 3
    X-Kong-Upstream-Latency: 3
    X-Kong-Proxy-Latency: 2
    Via: kong/2.1.4
    ```


### 5.2. Restrição por IP e User-Agent
1. Vamos adicionar restrições por user-agent, neste exemplo não iremos aceitar requisições do `Mozila`:
    * Na tela do `KONGA` clique em `PLUGINS`
    * Clique em `ADD GLOBAL PLUGIN`
    * Clique na aba `Security`
    * Encontre `Booot detection` e clique em `ADD PLUGIN` e preencha o formulário:
        * No campo `deny` entre com o valor `^[Mm]ozilla`
        * No campo `allow` entre com o valor `^[Cc]hrome`
        * Clique em `ADD PLUGIN`
    * Agora, ao tentar acessar o link http://localhost:8000/mock-direto do `Mozila` será negado mas do `Chrome` será aceito.
    * O `Kong` utiliza PCRE (Perl Compatible Regular Expression) para as suas expressões regulares, você pode utilizar este [site](https://regex101.com/) para validar as suas expressões regulares.
1. Agora vamos adicionar uma restrição por ip para um determinado serviço. O [KONG] utiliza restrições tanto como whitelist quanto blacklist.
    * No `KONGA` clique no menu `SERVICES`
    * Clique no serviço `mock-web-bank`
    * Clique em `Plugins` e em `ADD PLUGIN`
    * Clique em `Security`
    * Encontre o item `Ip Restriction` e clique em `ADD PLUGIN`
    * No campo `denny` entre com o ip que deseja bloquear
    * Clique em `ADD PLUGIN`




## 6. Monitoramento

### 6.1 Rastreio Distribuído

Rastreio distribuído é em especial necessário quando você precisa analisar uma requisição feita em um ambiente distribuído, a onde geralmente uma requisição ao chegar no backend, gerará outras requisições. Em um ambiente como este, a identificação de um ponto de lentidão complexo, e, é em cenários como esse em que se faz necessário uma solução para rastreio distribuído. No exercício a seguir, configuraremos o `KONG` utilizar o `Zipkin`. O Zikin a seguir está armazenando os traces em memória (não recomendado para ambientes produtivos).

Seguiremos os seguintes passos para configurar um plugin de `rastreio distribuído`:

1. Vamos subir o nosso container zipkin:
   ```bash
    docker run -d --name zipkin --restart unless-stopped --network kong-net -p 9411:9411 openzipkin/zipkin:2
    ```
2. Na interface do `KONGA`, após o login:
    * Clique no menu `Plugins`
    * Clique na aba  `ADD GLOBAL PLUGINS`
    * Clique na aba `Analytics & Monitoring`
    * No item `Zipkin` clique em `ADD PLUGIN`
    * Preencha o formulário da adição do plugin com:
        * http endpoint: `http://zipkin:9411/api/v2/spans`
        * sample ratio: `1` (não utilizar este valor em produção)
1. Execute este endpoint que irá chamar os outros serviços:
   ```bash
    $ curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/sync'
    ```
1. Acesse o [Zipkin](http://localhost:9411/zipkin/) e:
    * Clique em `RUN QUERY`
    * Na lista de resultado, clique no botão `SHOW`
1. Agora já podemos remover o plugin do zipkin:
    * No `KONGA`, na tela de plugins, encontre o zikin e clique no botão `DELETE`
1. E parar o nosso container
   ```bash
    $ docker stop  zipkin
    ```

### 6.2. Analise

Uma solução de analise é necessária para uma melhor gerência das suas aplicações. O `Kong` nos auxiliará com seus plugins, que como uma solução de API Gateway é um bonus, talvez um dos maiores bonus de se utilizar uma solução deste tipo. Com soluções de analsie você pode acompanhar a frequência de uso, performance, a porcentagem de erros, entre outros.

Inicialmente utilizaremos `ELK`.O `logstash` para a coleta de logs, `ElasticSearch` para armazenamento e `Kibana` para visualização.

1. Para subir o ambiente `ELK` rode:
   ```bash
    docker-compose -f docker-analise/docker-compose.yml up -d
    ```
1. Verifique se os serviços estão disponíveis:
   ```bash
    docker-compose -f docker-analise/docker-compose.yml ps
    ```
    * O resultado esperado é semelhante a:
   ```bash
    elasticsearch   /tini -- /usr/local/bin/do ...   Up      0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp                        
    kibana          /usr/local/bin/dumb-init - ...   Up      0.0.0.0:5601->5601/tcp                                                
    logstash        /usr/local/bin/docker-entr ...   Up      0.0.0.0:5044->5044/tcp, 0.0.0.0:5555->5555/udp, 0.0.0.0:9600->9600/tcp
    ```    
1. Clique em `Plugins` na tela principal do `KONGA`
1. Clique em `ADD GLOBAL PLUGIN`
1. Clique na aba `Logging`
1. Clique em `Udp Log` e preencha:
    * No campo `host` informe o valor `logstash`
    * No campo `port` informe o valor `5555`
    * Nos campos restantes deixe o valor padrão e clique em `ADD PLUGIN`

1. Gere dados:
   ```bash
    foo(){
        curl --location --request GET 'http://localhost:8000/mock-direto'
        curl --location --request GET 'http://localhost:8000/beta/api/v1/fast-random'
        curl --location --request GET 'http://localhost:8000/beta/ping'
        curl --location --request GET 'http://localhost:8000/beta'
        curl --location --request GET 'http://localhost:8000/beta'
        curl --location --request GET 'http://localhost:8000/beta'
        curl --location --request GET 'http://localhost:8000/mock-web-bank'
        curl --location --request GET 'http://localhost:8000/omega'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/fast'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/fast-random'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/slow'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/slow-random'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/who-am-i'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/sync'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/async'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/random-status'
    }
    for i in {1..60}
    do
        foo &
    done
    ```
1. Crie um index no [KIBANA](http://localhost:5601)
    * Se aparecer qualquer apresentação do `KIBANA`, ignore
    * Expanda o menu da esquerda e no submenu `Kibana` clique em `Discover`
    * Clique em `Create index pattern`
    * Na tela `Create index pattern`:
        * No campo `` informe o valor `logstash*`
        * Clique em `Next step`
        * No campo `Time field` selecione `@timestamp`
        * Clique em `Create index pattern`    
1. Visualize a quantidade total de dados:
    * Expanda o menu lateral esquerdo e no submenu `Kibana` clique em `Discover`
1. Crie uma visualização de requisição por período:
    * Expanda o menu lateral esquerdo e no submenu `Kibana` clique em `Visualize`
    * Clique em `Create new visualization`
    * Clique em `Line`
    * Selecione `logstash*`
    * Em `Buckets` clique em `Add` e selecione `X-axix`
        * Em `Aggregation` selecione `Data Histogram`
        * Deixe os outros campos com o valor padrão e clique no botão `Update`
    * Desça a tela e clique em `Add` e selecione `Split-series`
        * Em `sub-aggregation` selecione `Terms`
        * Em `Field` selecione `request.uri.keyword`
        * Clique no botão `Update`
    * Clique em `Save` e de o nome de `Kong - Por período de tempo` 
1. Crie um totalizador de requisições por endpoint
    * Expanda o menu lateral esquerdo e no submenu `Kibana` clique em `Visualize`
    * Clique em `Create new visualization`
    * Clique em `Data Table`
    * Selecione `logstash*`
    * Em `Buckets` clique em `Add` e selecione `Split rows`  
        * Em `aggregation` selecione `Terms`
        * Em `Field` selecione `request.uri.keyword`
        * Marque a opção `Group other values in separate bucket`
        * Em `Size` informe `10`
        * Em `Custon label` informe o valor `Endpoint` e clique em `Update`
    * Clique em `Save` e de o nome de `Kong - Requisições por endpoint`
1. Crie um mapa de pico de acesso por serviço
    * Expanda o menu lateral esquerdo e no submenu `Kibana` clique em `Visualize`
    * Clique em `Create new visualization`
    * Clique em `Heat Map`
    * Selecione `logstash*`
    * Em `Buckets` clique em `Add` e selecione `X-axis`  
        * Em `Aggregation` selecione `Data Histogram`
        * Em `Minimium Interval` entre com o valor `15m`
    * Em `Buckets` clique em `Add` e selecione `X-axis`
        * Em `Sub aggregation` selecione `Terms`
        * Em `Field` selecione `service.name.keyworkd`
        * Clique na aba `options`
        * Em `color schema` selecione `Yellow to Red`
    * Clique em `Save` e de o nome `Kong - Pico de acesso por serviço`
1. Crie um gráfico de status http
    * Expanda o menu lateral esquerdo e no submenu `Kibana` clique em `Visualize`
    * Clique em `Create new visualization`
    * Clique em `Pie`
    * Selecione `logstash*`
    * Em `Buckets` clique em `Add` e selecione `Split spaces`  
        * Em `Aggregation` selecione `Terms`
        * Em `Field` selecione `response.status`
        * Em `Size` informe `10`
    * Clique em `Save` e de o nome de `Kong - HTTP Status`
1. Organize todas as suas visualizações em um dashboard
    * Expanda o menu lateral esquerdo e no submenu `Kibana` clique em `Dashboard`
    * Clique em `Create new Dashboard`
    * Clique em `Add an existing` no centro esquerdo da tela e :
        * Na tela `Panel` selecione uma vez todos os itens
    * Clique em `Save` e de o nome de `Kong dashboard`
1. Limpe os containers de analise
   ```bash
    docker-compose -f docker-analise/docker-compose.yml down
    ```
1.Remova o plugin de `udp-log` na tela de `Plugins` no `KONGA`



### 6.3 Monitoramento do Kong
O Kong na versão paga tem um sistema de monitoramento de sinais vitais pronto. Aqui iremos mostrar uma solução open sourcer, que o template(no `Grafana`) é mantido pela comunidade oficial do `KONG`. Iremos utilizar o `Prometheus` para monitorar o `KONG` e o `Grafana` para exibir os nossos gráficos. 


1. Inicie os containers de monitoramento
   ```bash
    docker-compose -f docker-monitoramento/docker-compose.yml up -d
    ```
1. Validar implantação:  
   ```bash
    docker-compose -f docker-monitoramento/docker-compose.yml ps
    ```  
    * Resultado esperado:
   ```bash
    Name                 Command               State           Ports         
    ----------------------------------------------------------------------------
    grafana      /run.sh                          Up      0.0.0.0:3000->3000/tcp
    prometheus   /bin/prometheus --config.f ...   Up      0.0.0.0:9090->9090/tcp
    ```  
1. Adicionar plugin:
    * Na tela `Plugins` clique em `ADD GLOBAL PLUGIN`
    * Clique na aba `Analytics & Monitoring`
    * Em `Prometheus` clique em `ADD PLUGIN`
    * Clique em `ADD PLUGIN`
1. Gere dados:
   ```bash
    foo(){
        curl --location --request GET 'http://localhost:8000/mock-direto'
        curl --location --request GET 'http://localhost:8000/beta/api/v1/fast-random'
        curl --location --request GET 'http://localhost:8000/beta/ping'
        curl --location --request GET 'http://localhost:8000/beta'
        curl --location --request GET 'http://localhost:8000/beta'
        curl --location --request GET 'http://localhost:8000/beta'
        curl --location --request GET 'http://localhost:8000/mock-web-bank'
        curl --location --request GET 'http://localhost:8000/omega'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/fast'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/fast-random'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/slow'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/slow-random'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/who-am-i'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/sync'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/async'
        curl --location --request GET 'http://localhost:8000/omega/api/v1/chain/random-status'
    }
    for i in {1..1000}
    do
        foo &
    done
    ```
1. Entre no [Prometheus](http://localhost:9090/) e:
    * Em `Expresion` entre com o valor `kong_latency_count`
    * Clique em `execute`
1. Agora vamos entrar no [Grafana](http://localhost:3000/) e procurar por melhores visualizações:
    * Logue. `usuário` e `senha` são `admin`
    * Na próxima tela clique em `skip`
    * Coloque o mouse no ícone de engrenagem, posicionada no menu a esquerda. Clique na opção `Data Sources`
    * Clique em `Add data source`
    * Em `Prometheus` clique em `Select`
        * No campo `name` entre com o valor `kong-prometheus`
        * No campo `url` entre com o valor `http://prometheus:9090
        * No campo `Scrape interval` entre com o valor `10s`
        * No campo `Query timeout` entre com o valor `50s`
        * Clique em `Save & Test`
    * No menu `Dashborad` selecione `Manage`
    * Clique em `Import`
    * No campo `Import via grafana.com` entre com o valor `7424` e clique em `load`
    * Clique no botão `Change uid`
    * No campo `Unique identifier` entre com o valor `mock-kong-dashboard`
    * No campo `Phometheus` selecione `kong-prometheus`
    * Clique em `Import`
1. Vamos criar um alerta para quando as requisições se mantiverem a acime de 10 requisições por 10 segundos em um espaço de tempo de 20 segundos.
    * Se ainda não tiver, [crie um conta no slack](https://slack.com/signin#/signin)
    * Após confirmar a conta, crie o canal `kong-alerta`:
        * Clique em `Adicionar canais` e criar novo canal
        * No campo `Nome` entre com o valor `kong-alerta` e clique em `Criar`
    * Crie uma URL Webhook:
        * [novo app no slack](https://api.slack.com/apps?new_app=1)
        * Em `App Name` informe `Mock Kong`
        * Em `Development Slack Workspace` selecione conforme o que informou no seu cadastro
        * Clique em `Create App`
        * Clique em `Activate Incoming Webhooks` e então marque como `ON`
        * Clique no bortão `Add New Webhook to Workspace`
        * Em `Onde Mock Kong deve postar?` marque `kong-alerta` e clique em `Permitir`
        * Na lista de `Webhook URL` encontre o registrou que acabou de criar e clique em `Copy` e salve este valor em algum lugar
    * No `Grafana` passe o mouse sobre o item `Alert` e selecione `Notification Chanel`
    * Clique em `Add channel` e:
        * Em `name` entre com o valor `kong-slack`
        * Em `type` selecione `slack`
        * Em `url` cole o valor recuperado do `slack` em `Webhook URL`
        * Clique em `Test`
        * Verifique o seu canal no `Slack` se você recebeu a mensagem
        * Clique em `Save`
    * No `Grafana`, na tela de principal, em `Recently viewed dashboards` selecione `Kong(official)`
    * No dashborad expanda o item `Request Rate`
    * No item `Total requests per second (RPS)` clique em expandir e selecione `Edit`
        * Clique na aba `Alert`
            * Se for apresentado o erro `Template variables are not supported in alert queries` clique na aba `Query`
            * Verifique se o campo `Metrics` tem o valor `sum(rate(kong_http_status[1m]))`
            * Clique na aba `Alert` novamente
        * Clique em `Create Alert`
            * Em `Evaluete query` entre com o valor `10s` e em `For` com o valor `20s`
            * Em `conditions` no campo `IS ABOVE` entre com o valor `10`
            * Em `notifications` no campo `Send to` selecione `kong-slack` e em `Message` entre com o valor `Alto número de requisição`
            * Clique em `Save` no canto superior direito
            * Clique em `Save` no novo modal que apareceu
1. Provoque um alerta
   ```bash
    foo(){
        curl --location --request GET 'http://localhost:8000/beta/ping'
        curl --location --request GET 'http://localhost:8000/omega/ping'
    }
    for i in {1..10000}
    do
        foo &
    done
    ```
    * É esperado uma nova mensagem no canal do slack, semelhante a:
   ```bash
    [Alerting] Total requests per second (RPS) alert
    [Alerting] Total requests per second (RPS) alert
    Alto número de requisição
    Requests/second
    119.45833333333
    Grafana v7.2.2Grafana v7.2.2 | Hoje às 11h41
     ```
1. Limpe os containers de monitoramente
   ```bash
    docker-compose -f docker-monitoramento/docker-compose.yml down
    ```
1.Remova o plugin `prometheus` na tela de `Plugins` do `KONGA`

## 7. Boas Práticas em Implantação

O `Kong` é nativamente preparado para nuvem. Ele é uma camada distribuída de abstração, possuindo uma boa performance e capacidade de ser extensível. Com esta camada, você poderia evitar a replicação de algumas lógicas em seus diversos serviços, delegando a ele tarefas como limites de requisição, monitoramento, controle de acesso, autenticação, entre outros.
Por ser preparado nativamente para cloud e ter uma grande comunidade, hoje o `KONG` já conta com facilitadores pré definidos para cada cenário. 
Segue uma lista de guias direto da documentação do `KONG`

- [Kong Docker](https://github.com/Kong/docker-kong): Um arquivo Dockerfile para rodar o `KONG`
- [Kong como pacotes](https://github.com/Kong/kong/releases): Pacotes pré construídos para Debian, RedHat, entre outros.
- [Kong Vagrant](https://github.com/Kong/kong-vagrant): Um arquivo Vagrantfile para provisionamento de um ambiente pronto para `KONG`.
- [Kong Homebrew](https://github.com/Kong/homebrew-kong): Uma receita de Homebrew para `KONG`.
- [Kong CloudFormation](https://github.com/Kong/kong-dist-cloudformation): Facilita o deploy do `Kong` na AWS EC2.
- [Kong AWS AMI](https://aws.amazon.com/marketplace/pp/B06WP4TNKL): Kong AMI na AWS Marketplace.
- [Kong on Microsoft Azure](https://github.com/Kong/kong-dist-azure): Rodar o `KONG` usando Azure Resource Manager.
- [Kong on Heroku](https://github.com/heroku/heroku-kong): Facilita o deploy do `KONG` no
  Heroku.
- [Kubernetes Ingress Controller for Kong](https://github.com/Kong/kubernetes-ingress-controller): Utilizar o Kong através da especificação Ingress do Kubernetes.

O que é importante frisar é como garantir a replicabilidade em um ambiente. Para cada um desses cenários, sem fazer o mérito de qual é melhor ou pior, existe uma possibilidade. Transformando o seu ambiente em algo replicável, ou seja, escrevendo o seu ambiente. Daqui vem a definição de infraestrutura como código(IaC).

Para o `Kong` na maioria dos cenários, você pode garantir a replicabilidade do ambiente através de caminhos de ci/cd. Nele, você poderia tratar o Kong como mais uma `API` e que você quer garantir o estado. Nessa abordagem, uma boa solução para gerenciar o estado dele é o [deck](https://docs.konghq.com/deck/overview/) (veja o item 2.2 para exemplo). Neste cenário, seriam necessário um ambiente de qualidade para antecipar mudanças e testes. Fazer um dê para do arquivo salvo, atualizando ali quais seriam os valores para o próximo ambiente (através de variáveis em seu pipeline).

Uma outra solução para o `Kong` seria tratá-lo como uma aplicação que tem parâmetros de configuração em um banco de dados, garantindo aqui então o estado do banco de dados. Não indico essa abordagem caso não se tenha interesse em acompanhar a comunidade do `Kong` para prever grandes alterações de versão do `Kong` e incompatibilidades, isso comprometeria o seu gerenciamento de estado. Ainda nesta abordagem, um grande nome em gerenciamento de estado de banco de dados é o [Liquibase](https://www.liquibase.org)

Outra forma de tratar o `Kong`, caso rode na plataforma `Kubernetes` é através do Kong Ingress. Neste cenário você poderia utilizar uma esteira de ci/cd e através de Iac para `Kubernetes`, como o [Helm chart]((https://helm.sh/). Com ele você poderia garantir o estado do `Kong` através de arquivos `values.yml` com as variáveis de cada ambiente. Neste cenário se faria até desnecessário usar o `Kong`com um banco de dados, pois as suas configurações  já estariam na plataforma `Kubernetes`.  